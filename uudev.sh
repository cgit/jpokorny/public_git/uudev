#/bin/bash

# uudev.sh
# Stupid configure-by-modification udev rule engine for ordinary users as udev
# is not and will not be user-friendly even though there is hardly any
# conceptual obstacle: <https://bugzilla.redhat.com/show_bug.cgi?id=1248074>.
#
# Search for "CUSTOM SECTION" as a modification start point.
# You may also need to tweak udevadm monitor invocation at the bottom.
#
# Copyright 2015 jpokorny@fedoraproject.org
# Distributed under terms of GPLv2 license

exec <&-
set -eu

declare -e holdtime=10

main() {
	local line
	declare ACTION=
	declare BUSNUM=
	declare DEVNAME=
	declare DEVNUM=
	declare DEVPATH=
	declare DEVTYPE=
	declare INTERFACE=
	declare MAJOR=
	declare MINOR=
	declare MODALIAS=
	declare PRODUCT=
	declare SEQNUM=
	declare SUBSYSTEM=
	declare TYPE=
	
	while read line; do
		#echo "LINE: ${line}"
		case "${line}" in
		# important lines
		ACTION=*) ;&
		BUSNUM=*) ;&
		DEVNAME=*) ;&
		DEVNUM=*) ;&
		DEVPATH=*) ;&
		DEVTYPE=*) ;&
		INTERFACE=*) ;&
		MAJOR=*) ;&
		MINOR=*) ;&
		MODALIAS=*) ;&
		PRODUCT=*) ;&
		SEQNUM=*) ;&
		SUBSYSTEM=*) ;&
		TYPE=*) ;&
		__MARK__=__SENTINEL__)
			#echo "local ${line}"
			declare ${line}
			;;
		# skip lines
		KERNEL*) ;&
		*monitor\ will\ print\ the\ received\ events\ for*)
			;;
		*)
			# for some reason a backspace is present in the "empty line"
			# denoting end of event block
			python -c "from sys import exit; exit ([ord(c) for c in r'''${line}'''] == [10])" \
			  && { systemd-cat -t uudev <<< "unrecognized ${line}"; continue; } \
			  || : 
			declare script=
			case "${PRODUCT}" in
			"")
				;;
# CUSTOM SECTION BEGIN
			58f/9410/122*)
				script="${HOME}/.udev/keyboard"
				;;
# CUSTOM SECTION END
			*)
				systemd-cat -t uudev <<< "unhandled PRODUCT: ${PRODUCT}"
				;;
			esac
			test -z "${script}" && continue || :
			test -x "${script}" || { systemd-cat -t uudev <<< "access error: ${script}"
			                         continue; }
			test "$(($(stat -c %X -- "${script}") + holdtime))" -lt "$(date +%s)" \
			  || { systemd-cat -t uudev <<< "skipping possibly repeated action: ${script}:${ACTION}"
		               continue; }
			systemd-cat -t uudev <<< "executing: ${script} ${ACTION}"
			touch -a "${script}"
			${script} ${ACTION} || systemd-cat -t uudev <<< "error executing: ${script} ${ACTION}"
			;;
		esac
	done < <(script -q -c 'udevadm monitor -s usb/usb_interface -kp')
}

main
